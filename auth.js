//[SECTION]Dependencies and Modues
	//Produce Accss Tokens using JWT
	const jwt = require('jsonwebtoken');
	const dotenv = require('dotenv');

//[SECTION]Environment Setup
	dotenv.config();
	let secret = process.env.SECRET;

//[SECTION]Functionalities
	//Create Access Token
	module.exports.createAccessToken = (authUser) => {
	let  userData = {
			id: authUser._id,		
			email: authUser.email,
			isAdmin:authUser.isAdmin,
			}
		return jwt.sign(userData, secret, {});
	};

	//Create verify token
	module.exports.verify = (req, res, next) => {
		let token = req.headers.authorization;
		if (typeof token !== 'undefined' ) {
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (err, payload) =>{
				if (err) {
					return res.send({auth: 'Auth Failed Token invalid'})
				} else {
					next();
				}
			});
		} else {
			return res.send({auth: "Authorization Failed,Check Token"})
		};
	};

	//decode function
	module.exports.decode = (accessToken) => {
		if (typeof accessToken !== 'undefined') {
			accessToken = accessToken.slice(7, accessToken.length)
			return jwt.verify(accessToken, secret, (err, payload)=>{
				if (err) {
					return null;
				} else {
					return jwt.decode(accessToken, {complete: true}).payload;
				};
			});
		} else {
			return null;
		}
		
	};