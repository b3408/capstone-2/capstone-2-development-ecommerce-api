//[SECTION]Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');

//[SECTION]Routing Component
	const route = express.Router();

//[SECTION] ROUTE [POST]
	//create a product.
	route.post('/createproduct', auth.verify,(req,res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ? controller.createProduct(req.body).then(outcome => {
			res.send(outcome)
		})
		: res.send('unauthorized access')
	});

	//retrieve active product
	route.get('/activeproducts', (req, res) => {
		controller.retrieveActiveProduct(req.body).then(result => {
			res.send(result)
		});
	});

		//retrieve active product
	route.get('/allproducts', (req, res) => {
		controller.retrieveAllProduct(req.body).then(result => {
			res.send(result)
		});
	});

	//retrieve single product
	route.get('/:productId', (req, res) => {
		controller.retrieveProduct(req.params.productId).then(result => {
			res.send(result)
		});
	});

//[SECTION] ROUTE [PUT]
	//update a product [Admin]
	route.put('/:productId', auth.verify,(req,res) =>{
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ? controller.updateProduct(req.params, req.body).then(result => {res.send(result)}):res.send('unauthorized access')
	});

	//archive a product
	route.put('/:productId/archive', auth.verify,(req,res) =>{
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ? controller.archiveProducts(req.params, req.body).then(result => {
			res.send(result)
		})
		:res.send ('Unauthorized access')
	})

		route.put('/:productId/unarchive', auth.verify,(req,res) =>{
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ? controller.unArchiveProducts(req.params, req.body).then(result => {
			res.send(result)
		})
		:res.send ('Unauthorized access')
	})

		


//[SECTION] Delete product
		route.delete('/:productId/delete', auth.verify,(req,res) =>{
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin
		let id = req.params.productId
		 isAdmin ?
		controller.deleteProduct(id).then(result =>{
			res.send(result);
		})
		: res.send('unauthorized user')
	});

//[SECTION]EXPOSE ROUTE
	module.exports = route;