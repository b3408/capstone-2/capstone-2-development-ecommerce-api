//[SECTION]Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');


//[SECTION]Routing Component
	const route = express.Router();

//[SECTION]Routes [POST]
	route.post('/register', (req,res) => {
		let data = req.body
		controller.createUser(data).then(outcome => {
			res.send(outcome)
		});
	});

//Login [User] 
	route.post('/login', (req,res) => {
			let data = req.body
			controller.loginUser(data).then(result =>{
				res.send(result)
			});
	});	

//change pass
	route.post('/changepass', (req,res) => {
			let data = req.body
			controller.changePass(data).then(result =>{
				res.send(result)
			});
	});		
//order product
	route.post('/order', auth.verify,(req,res) =>{

			let userId = auth.decode(req.headers.authorization).id;
			let email = auth.decode(req.headers.authorization).email;
			let isAdmin = auth.decode(req.headers.authorization).isAdmin; 
			let data = {
				userId: userId,
				productId: req.body.productId,
				quantity: req.body.quantity,
				email: email,
			};

			if (!isAdmin) {
				controller.createOrder(data).then(outcome =>{
				
					res.send(outcome);

				}) 
			} else {
				res.send('unauthorized')
			};
		})

	// retrieve all orders
	route.get('/getallusers', auth.verify,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ? controller.getAllUsers().then(result => {
			res.send(result) 
		})
		: res.send ('Unauthorized access')
	});
	
// retrieve all orders
	route.get('/getallorders', auth.verify,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ? controller.getAllOrders().then(result => {
			res.send(result) 
		})
		: res.send ('Unauthorized access')
	});

// retrieve user order
route.get('/getorders', auth.verify,(req,res) =>{

			let userId = auth.decode(req.headers.authorization).id;
			let isAdmin = auth.decode(req.headers.authorization).isAdmin; 
			let data = {
				userId: userId,
				productId: req.body.productId,
			};

			if (!isAdmin) {
				controller.retrieveOrders(data).then(outcome =>{
				
					res.send(outcome);

				}) 
			} else {
				res.send('unauthorized')
			};
		})

//[SECTION] Routes - [GET]
	route.get('/details', auth.verify,(req,res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(result =>{
			res.send(result);
		});
	});
	
//[SECTION] Routes - [UPDATE]

	route.put('/:userId/set-as-admin', auth.verify,(req,res) =>{
			let isAdmin = auth.decode(req.headers.authorization).isAdmin
		isAdmin ?controller.setAsAdmin(req.params.userId).then(result => res.send(result)): res.send('unauthorized')
	});

		//set user as non admin
	route.put('/:userId/set-as-user', auth.verify ,(req, res) => {
   	  	let isAdmin = auth.decode(req.headers.authorization).isAdmin
   	   isAdmin ? controller.setAsNonAdmin(req.params.userId).then(result => res.send(result)) 
   	   : res.send('Unauthorized User');  	   
   });
		
		
//[SECTION] Delete product
		route.delete('/:userId/delete', auth.verify,(req,res) =>{
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin
		let id = req.params.userId
		 isAdmin ?
		controller.deleteUser(id).then(result =>{
			res.send(result);
		})
		: res.send('unauthorized user')
	});


//[SECTION] ROUTE EXPOSE
	module.exports = route;
