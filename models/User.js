//[SECTION]Dependencies and modules
const mongoose = require('mongoose');

//[SECTION]Schema Blueprint
const userBlueprint = new mongoose.Schema({
	userName: {
		type : String,
		required : [true, 'First name is required']
	},
	firstName: {
		type : String,
		required : [true, 'First name is required']
	},
	lastName: {
		type : String,
		required : [true, 'Last name is required']
	},
	middleName: {
		type : String,
		required : [true, 'Middle name is required']
	},
	address: {
		type : String,
		required : [true, 'First name is required']
	},
	email: {
		type : String,
		required : [true, 'Email is required']
	},
	password: {
		type : String,
		required : [true, 'Password is required']
	},
	isAdmin: {
		type : Boolean,
		default : false
	},
	orders : [
	{
		productId:{
			type : String,
			required : [true, 'Product Id required']
		},
		productName:{
			type : String,
			required : [true, 'Product Id required']
		},
		price:{
			type : String,
			required : [true, 'Product Id required']
		},			
		quantity:{
			type : Number,
			required : [true, 'Product Id required']
		}
		,		
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		TotalAmount : {
			type: Number,
			required: [true, 'Total amount is required']

		}
	}
	],
	
});

//[SECTION]Export Models
module.exports  = mongoose.model('User', userBlueprint);