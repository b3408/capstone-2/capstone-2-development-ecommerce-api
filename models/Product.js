//[SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

//[SECTION] Schema for Products
	const productSchema = new mongoose.Schema({
		name:{
				type:String,
				required : [true, "Name is Required"]
		},		
		description:{
				type: String,
				required : [true, "Description is Required"]
		},
		brand:{
			type: String,
			required : [true, "Brand is Required"]
		},
		price:{
				type: Number,
				required : [true, "Price is Required"]
		},
		isActive:{
				type: Boolean,
				default : true
		},
		createdOn:{
				type: Date,
				default : new Date()
		},
		orders:[
		{
			customerId:{
				type: String,
				required: [true, "customerId's ID is required"]
			},
			email:{
				type: String,
				required: [true, "Customer email is required"]
				}
		}] 
	});

	module.exports  = mongoose.model('Product', productSchema);
