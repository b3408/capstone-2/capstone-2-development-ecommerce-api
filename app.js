//[SECTION]Dependencies and modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const usersRoutes = require('./routes/users')
	const productsRoutes = require('./routes/products')
	


//[SECTION]Environment Variables
	dotenv.config();
	//config var
	const port = process.env.PORT;
	const mongodb = process.env.MONGO_DB;

//[SECTION]Server Setup
	const server = express();
	server.use(cors());
	server.use(express.json());
	server.use(express.urlencoded({extended: true}));

//[SECTION]Database Connect
	mongoose.connect(mongodb);
	let db = mongoose.connection;
	db.once("open", () =>{
		console.log('You are now connected to mongoDb Atlas')
	})

//[SECTION]Server routes
	server.use('/users', usersRoutes)
	server.use('/product', productsRoutes)
	
//[SECTION]Server Response
	server.listen(port, () =>{
		console.log(`API is now online on port ${port}`)
	})

	server.get('/', (req,res)=>{
		res.send('Ongoing Construction Capstone2 Project ')
	})