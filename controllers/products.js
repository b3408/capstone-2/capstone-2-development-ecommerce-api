//[SECTION] Modules and dependencies
		const User = require('../models/User');
		const Product = require('../models/Product');
		const bcrypt = require('bcrypt');
		const auth = require('../auth');

//[SECTION]FUNCTION Create
		module.exports.createProduct = (prodObj) => {
		let newProduct = new Product({
			name : prodObj.name,
			description : prodObj.description,
			brand : prodObj.brand,
			price : prodObj.price
		});
		return newProduct.save().then((result,err )=> {
			if (err) {
				return false;
			} else {
				return newProduct
			}
		});
	};

//[SECTION]FUNCTION Retrieve 
	//Retrieve All Active Products
		module.exports.retrieveActiveProduct = (prodObj) => {
			return Product.find({isActive: true}).then(result => {	
			return result		
			});
		};	

//[SECTION]FUNCTION Retrieve 
	//Retrieve All  Products
		module.exports.retrieveAllProduct = (prodObj) => {
			return Product.find({}).then(result => {	
			return result		
			});
		};	



	//Retrieve single product
		module.exports.retrieveProduct = (id) => {
			return Product.findById(id).then((result, err) => {
				if (result) {
					return result
				} else {
					return 'product not found'
				}
			});
		};

	//Retrieve All Active Products
	

//[SECTION]FUNCTION Update
	//update a product information (admin)
		module.exports.updateProduct = (product, prodObj) => {
			let updatedProduct = {
				name : prodObj.name,
				description : prodObj.description,
				price : prodObj.price,
			}
			let id = product.productId
			return Product.findByIdAndUpdate(id, updatedProduct).then((result, err) => {
					if (result) {
						return result
					} else {
						return `error updating the product`
					}
			});
			};

//[SECTION]FUNCTION Archive

	module.exports.archiveProducts = (product, prodObj) => {
		let newStatus = {
			isActive : false
		}
	
			let id = product.productId
		return Product.findByIdAndUpdate(id, newStatus).then((result, err) =>{
			if (result) {
				return result
			} else {
				return false
			}
		})
	};

//[SECTION]FUNCTION Archive

	module.exports.unArchiveProducts = (product, prodObj) => {
		let newStatus = {
			isActive : true
		}
			let id = product.productId
		return Product.findByIdAndUpdate(id, newStatus).then((result, err) =>{
			if (result) {
				return result
			} else {
				return false
			}
		})
	};




	//Delete Product 
	module.exports.deleteProduct = (prodObj) =>{
			return Product.findByIdAndRemove(prodObj).then((delProd, err)=>{
				if (delProd) {
					return `successfully remove ${delProd}`
				} else {
					return 'Successfully delete product'
				}
			});
	};