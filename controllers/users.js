//[SECTION]Dependencies and modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const bcrypt = require('bcrypt');
	const auth = require('../auth')
	const nodemailer = require('nodemailer')
	const Order = require('../models/Product');
	const _ = require('lodash')

//[SECTION]Nodemailer config
let transporter = nodemailer.createTransport({
		service: "yahoo",
		auth: {
			user: "jamesdgdavid@yahoo.com",
			pass: "bhholwweoiyflbox"
		},
		tls: {
			rejectUnauthorized : false,
		}
	})




//[SECTION]FUNCTION create user with checking username
	module.exports.createUser = async (data) => {
		let checkUser = await User.findOne({userName :data.userName} ).then(result =>{
			if (!result) {	
			return User.findOne({email :data.email}).then(result => {
				if (!result) {
				let newUser = new User({
				userName: data.userName,
				firstName: data.firstName,
				lastName: data.lastName,
				middleName: data.middleName,
				address: data.address,
				email: data.email,
				password: bcrypt.hashSync(data.password, 10)
				});
				return newUser.save().then ((success, err) => {
					if (success) {
						let sendConfirmation  =  transporter.sendMail ({
							from: "jamesdgdavid@yahoo.com",
							to: data.email,
							subject: "Registration Successfully",
							text: "Hello World"	
						});
					console.log('Send Email Confirmation')
					return success;
					} else {
					return false;		
					  }
					 	});
				}else {
					 };
				});
			} else {
				return false ;
			};
		});
			if (checkUser) {
				return checkUser
			} else {
				return 'Username or Email Exist'
			}
	};

	//Login (User Authentication)
	module.exports.loginUser = (userObj) => {
		return User.findOne({userName: userObj.userName}).then(result => {
			if (result === null) {
					return false
			} else {
			let passW = result.password; //colection
					const isMatched = bcrypt.compareSync(userObj.password, passW);
					if (isMatched) {
						let dataNiUser = result.toObject()
						return{access: auth.createAccessToken(dataNiUser)};
					} else {
						return false
					}
			};
		});
	};
	//change password 
	module.exports.changePass = (userObj) => {
		return User.findOne({userName: userObj.userName}).then(result => {
			if (result === null) {
					return 'Username does not exist'
			} else {
			let passW = result.password; //colection
					const isMatched = bcrypt.compareSync(userObj.password, passW);
					if (isMatched) {
						let newPass = {
							password : bcrypt.hashSync(userObj.newpassword, 10)
						}
						return User.findOneAndUpdate({userName: userObj.userName}, newPass).then((res, err) =>{
							if (res) {
								return res
							} else {
								return false;
							}
						})
					} else {			
						return false
					};
				};
			});
		};

	 module.exports.createOrder = async (data) => {
      let id = data.userId;
      let product = data.productId;
      let quantity = data.quantity
 	
  		let total = await Product.findById(product).then(product => {
  			let productId = product.id
  			let prodName = product.name
  			let prodPrice = product.price
  		

  		let userCreateOrder = User.findById(id).then(user => {
  			let TotalAmount = user.orders.reduce((total) => {
				  
  					return total + (product.price * quantity)
  			},0)
  				
  			let newData = {
  				  productId : productId,
  				  price : prodPrice,
  				  productName: prodName , 
  				  quantity : quantity, 
  				 
  			}
  			console.log(newData)
  			user.orders.push({TotalAmount:TotalAmount, productId :newData.productId, price: newData.price, productName:newData.productName, quantity: newData.quantity}  )
  			return user.save().then((saved,err) => {
  				if (saved) {
  					
  					return saved
  				} else {
  					return false
  				}
  			})
  		});
  		return true
 	})
    let isProductUpdated = await Product.findById(product).then(products => {
          	products.orders.push({customerId: id, email : data.email});
           return products.save().then((saved, err) => {
              if (err) {
                 return false;
              } else {
            
                 return saved; 
              }; 
           });
     });
   
    return total && isProductUpdated
  
  };



  //[SECTION]Functionality [Retrieve]
	module.exports.getProfile = (id) => {
			return User.findById(id).then(user => {
				return user;
			})
	}

	 //[SECTION]Functionality [Retrieve All User]
	module.exports.getAllUsers = (userObj) => {
			return User.find({}).then((res,err )=> {
				if (res) {
					return res
				} else {
					return false
				}
			})
	};

	//Retrieve All Orders[ADMIN]
		module.exports.getAllOrders = (dataObj) => {
			return Product.find({}).then((res, err) =>{
				if (res) {
					return res
				} else {
					return false
				}
			})
		
		};	

	//Retrieve user order

	 module.exports.retrieveOrders = async (data) => {
      let id = data.userId;

 		let showOrders = await User.findById(id).then(user =>{
 			 return user
 		});
 	return showOrders
 	}
  	
	//[SECTION]Functionality [Update]
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin : true
		}
		return User.findByIdAndUpdate(userId, updates).then((result, err) => {
			if (result) {
				return `Successfully update user role as Admin`;
			} else {
				return `Updates Failed to implement`
			}
		});
	};
	
	//Set as non admin
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin : false
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return `Successfully demote user role as User`;;
			} else {
				return `Updates Failed to implement`
			};
		});
	};


	


//[SECTION]FUNCTION DELETE
//Deleete USER
	module.exports.deleteUser = (userObj) =>{
		console.log(userObj)
			return User.findByIdAndRemove(userObj).then((result, err)=>{
				if (result) {
					return result
				} else {
					return err
				}
			});
	};
